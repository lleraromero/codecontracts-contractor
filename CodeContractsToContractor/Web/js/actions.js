﻿$(document).ready(function () {
    RequestWebServiceVersion();
    // Now that the DOM is fully loaded I can setup the event listeners.
    Dropzone.options.myDrop = {
        init: function () {
            //File successfuly uploaded, now I have to process the contract.
            this.on("success", function (file, response) {
                //Update the modal with the classes available in this assembly.
                UpdateModal(response);
                $('#modalClasses').modal({ backdrop: 'static', keyboard: false }).modal('show');

                $('#modalClasses').one('hide.bs.modal', function () {
                    var uploader = Dropzone.forElement("#myDrop");
                    $('#assemblyClasses').find(":selected").each(function (index, elem) {
                        var selectedClass = $(elem).text();
                        response.SelectedClass = selectedClass;

                        if (index == 0) {
                            $(file.previewTemplate).find('.dz-filename').html("<span data-dz-name=\"\">" + selectedClass + "</span>");
                        } else if (index > 0) {
                            //Creating a mock for each extra class.
                            file = { name: selectedClass, size: file.size };
                            // Call the default addedfile event handler
                            uploader.options.addedfile.call(uploader, file);
                        }

                        var spinner = CreateSpinner(file);
                        RequestTypeState(file, response, spinner);
                    });
                });
            });
        }
    }

    function RequestWebServiceVersion() {
        $.ajax({
            type: "GET",
            url: "http://localhost:4635/api/status",
            dataType: "json",
            timeout: 8000,
        }).error(function () {
            $("#status").removeClass("label-success")
                        .addClass("label label-danger").text("OFFLINE");
        }).done(function (data) {
            $("#status").removeClass("label-danger")
                        .addClass("label label-success").text("ONLINE")
            $("#version").text(data.Version);
        });

        setTimeout(RequestWebServiceVersion, 10000);
    };

    $("#generate").click(function () {
        $('#modalClasses').modal('hide');
    });

    $("#example").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost:4635/api/files/examples-lafhis",
            dataType: "json"
        }).done(function (response) {
            UpdateModal(response);
            $('#modalClasses').modal({ backdrop: 'static', keyboard: false }).modal('show');

            $('#modalClasses').one('hide.bs.modal', function () {
                var uploader = Dropzone.forElement("#myDrop");
                $('#assemblyClasses').find(":selected").each(function (index, elem) {
                    var selectedClass = $(elem).text();
                    response.SelectedClass = selectedClass;

                    var file = { name: selectedClass, size: Math.random() * 100 };
                    // Call the default addedfile event handler
                    uploader.options.addedfile.call(uploader, file);

                    var spinner = CreateSpinner(file);
                    RequestTypeState(file, response, spinner);
                });
            });
        })
    });

    function CreateSpinner(file) {
        var spinner = new Spinner({ color: '#000000', lines: 12, className: 'spinner' });
        spinner.spin();
        $(file.previewTemplate).find('.dz-filename').append(spinner.el);

        return spinner;
    }

    function UpdateModal(response) {
        var select = $('#assemblyClasses');
        var options = (select.prop) ? select.prop('options') : select.attr('options');
        $('option', select).remove();
        $.each(response.File.Classes, function (val, text) {
            options[options.length] = new Option(text, val);
        });
        select.chosen({ width: "100%" });
        select.trigger("chosen:updated");
    }

    function RequestTypeState(file, response, spinner) {
        var filename = response.File.Name;
        var selectedClass = response.SelectedClass;
        $.ajax({
            type: "GET",
            url: "http://localhost:4635/api/files/" + filename + "/" + selectedClass,
            dataType: "json"
        }).always(function (request, status, error) {
            var image_name = filename + "-" + selectedClass + ".svg";
            (function pollTypestate() {
                $.ajax({
                    type: "GET",
                    url: "http://localhost:4635/api/files/status/" + image_name,
                    error: function (xhr, s, e) {
                        // If no image was found, retry, eventually it will appear :)
                        if (s === "timeout" || xhr.status == 404) {
                            setTimeout(pollTypestate, 9000);
                        }
                    },
                    success: function () {
                        var src = "http://localhost:4635/images/" + image_name;
                        var alt = filename + "-" + selectedClass;

                        //Update the thumbnail.
                        $(file.previewTemplate).removeClass("dz-file-preview").addClass("dz-image-preview");
                        var thumbnail = $(file.previewTemplate).find('img');
                        $(thumbnail).replaceWith("<img data-dz-thumbnail=\"\" style=\"cursor: pointer;background-color:white;border: solid 1px #ACACAC\" alt=\"" + alt + "\" src=\"" + src + "\"  />");
                        //Add the link to the LTS.
                        $(file.previewTemplate).find('img').click(function () {
                            window.open(src);
                        });
                        spinner.stop();
                    }
                })
            }());
        });
    }
});