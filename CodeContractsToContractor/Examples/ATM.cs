﻿using System.Diagnostics.Contracts;

namespace Examples
{
  public class ATM
  {
    #region Variables

    private bool theCardIn;
    private bool carHalfway;
    private bool passwordGiven;
    private int card;
    private int passwd;

    #endregion Variables

    #region Properties

    public bool TheCardIn
    {
      get { return theCardIn; }
      set { theCardIn = value; }
    }

    public bool CarHalfway
    {
      get { return carHalfway; }
      set { carHalfway = value; }
    }

    public bool PasswordGiven
    {
      get { return passwordGiven; }
      set { passwordGiven = value; }
    }

    public int Card
    {
      get { return card; }
      set { card = value; }
    }

    public int Passwd
    {
      get { return passwd; }
      set { passwd = value; }
    }

    #endregion Properties

    public ATM()
    {
      Contract.Ensures(!TheCardIn && !CarHalfway && !PasswordGiven);
      Contract.Ensures(Card == 0 && Passwd == 0);

      theCardIn = false;
      carHalfway = false;
      passwordGiven = false;
      card = 0;
      passwd = 0;
    }

    public void InsertCard(int c)
    {
      Contract.Requires(!TheCardIn && c > 0);
      Contract.Ensures(TheCardIn && Card == c);

      theCardIn = true;
      card = c;
    }

    public void EnterPassword(int q)
    {
      Contract.Requires(!PasswordGiven && q > 0);
      Contract.Ensures(PasswordGiven && Passwd == q);

      passwordGiven = true;
      passwd = q;
    }

    public void TakeCard()
    {
      Contract.Requires(CarHalfway);
      Contract.Ensures(!CarHalfway && !TheCardIn);

      carHalfway = false;
      theCardIn = false;
    }

    public void DisplayMainScreen()
    {
      Contract.Requires(!TheCardIn && !CarHalfway);
    }

    public void RequestPassword()
    {
      Contract.Requires(!PasswordGiven);
    }

    public void EjectCard()
    {
      Contract.Requires(TheCardIn);
      Contract.Ensures(!TheCardIn && CarHalfway && Card == 0 && Passwd == 0 && !PasswordGiven);

      theCardIn = false;
      carHalfway = true;
      card = 0;
      passwd = 0;
      passwordGiven = false;
    }

    public void RequestTakeCard()
    {
      Contract.Requires(CarHalfway);
    }

    public void CanceledMessage()
    {
      Contract.Requires(TheCardIn);
    }
  }
}