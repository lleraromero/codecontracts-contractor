﻿using System.Diagnostics.Contracts;

namespace Examples
{
  public class FiniteStack
  {
    #region Variables

    private int max;
    private int next;

    #endregion Variables

    #region Properties

    public int Max
    {
      get { return max; }
      set { max = value; }
    }

    public int Next
    {
      get { return next; }
      set { next = value; }
    }

    #endregion Properties

    public FiniteStack()
    {
      Contract.Ensures(Max == 5);
      Contract.Ensures(Next == -1);

      max = 5;
      next = -1;
    }

    public FiniteStack(int size)
    {
      Contract.Requires(size > 2);
      Contract.Ensures(Max == size);
      Contract.Ensures(Next == -1);

      max = size;
      next = -1;
    }

    [ContractInvariantMethod]
    private void Invariant()
    {
      Contract.Invariant(Max > 2 && Next >= -1 && Max > Next);
    }

    public void Pop()
    {
      Contract.Requires(Next >= 0);
      Contract.Ensures(Next == Contract.OldValue(Next) - 1);

      --next;
    }

    public void Push(int elem)
    {
      Contract.Requires(Max - 1 > Next);
      Contract.Ensures(Next == Contract.OldValue(Next) + 1);

      ++next;
    }
  }
}