﻿using System.Diagnostics.Contracts;

namespace API_Examples
{
  // Tambien funciona con un array generico de tipo T
  public class BoundedStack
  {
    public int[] elems;
    public int next;

    [ContractInvariantMethodAttribute]
    private void Invariant()
    {
      Contract.Invariant(this.elems != null);
      Contract.Invariant(this.next >= 0);
      Contract.Invariant(this.next <= this.elems.Length);
      Contract.Invariant(Contract.ForAll(0, elems.Length, i => elems[i] < 20));
    }

    public BoundedStack(int size)
    {
      Contract.Requires(size > 0);
      Contract.Ensures(elems.Length == size);
      Contract.Ensures(next == 0);
      this.elems = new int[size];
      this.next = 0;
    }

    public void Push(int elem)
    {
      Contract.Requires(this.next < this.elems.Length && elem < 20);
      Contract.Ensures(next == Contract.OldValue<int>(next) + 1);
      this.elems[this.next] = elem;
      this.next++;
    }

    public object Pop()
    {
      Contract.Requires(this.next > 0);
      Contract.Ensures(next == Contract.OldValue<int>(next) - 1);
      this.next--;
      return this.elems[this.next];
    }
  }
}