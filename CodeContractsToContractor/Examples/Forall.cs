﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Examples
{
    public class Forall
    {
        #region Variables

        private int max;
        private List<int> values;

        #endregion Variables

        #region Properties

        public int Max
        {
            get { return max; }
            set { max = value; }
        }

        #endregion Properties

        public Forall()
        {
            Contract.Ensures(Max == 10);

            max = 10;
        }

        public void Push(int elem)
        {
            Contract.Requires(elem > Max);
            Contract.Ensures(Contract.ForAll(values, x => x > Max));

            values.Add(elem);
        }
    }
}