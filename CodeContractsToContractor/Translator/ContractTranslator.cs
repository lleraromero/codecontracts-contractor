﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Xml;

namespace Translator
{
  public class ContractTranslator
  {
    private class ContractorElement
    {
      public string name;
      public Dictionary<string, Type> parameters;
      public string post;
      public string pre;
    }

    private string name;
    private string invariant;
    private Dictionary<string, Type> variables;
    private Dictionary<string, List<ContractorElement>> constructors;
    private Dictionary<string, List<ContractorElement>> methods;

    public ContractTranslator(string name, string invariant)
    {
      this.name = name;
      this.invariant = invariant;
      this.constructors = new Dictionary<string, List<ContractorElement>>();
      this.methods = new Dictionary<string, List<ContractorElement>>();
      this.variables = new Dictionary<string, Type>();
    }

    public void addConstructor(string name, string pre, string post, Dictionary<string, Type> parameters)
    {
      var element = new ContractorElement() { name = name, pre = pre, post = post, parameters = parameters };

      if (!constructors.ContainsKey(name))
        constructors[name] = new List<ContractorElement>() { element };
      else
      {
        element.name += constructors[name].Count + 1;
        constructors[name].Add(element);
      }
    }

    public void addMethod(string name, string pre, string post, Dictionary<string, Type> parameters)
    {
      var element = new ContractorElement() { name = name, pre = pre, post = post, parameters = parameters };

      if (!methods.ContainsKey(name))
        methods[name] = new List<ContractorElement>() { element };
      else
      {
        element.name += methods[name].Count + 1;
        methods[name].Add(element);
      }
    }

    public void addVariable(string name, Type type)
    {
      Contract.Requires(string.IsNullOrEmpty(name));
      Contract.Requires(type != null);

      if (!variables.ContainsKey(name))
        variables.Add(name, type);
    }

    private XmlDocument CreateContract(string name, string invariant)
    {
      XmlDocument xmlDoc = new XmlDocument();

      XmlNode contract = xmlDoc.CreateElement("contract");
      XmlAttribute attr = xmlDoc.CreateAttribute("name");
      attr.Value = name;
      contract.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("invariant");
      attr.Value = GetValueFromCondition(invariant);
      contract.Attributes.Append(attr);
      xmlDoc.AppendChild(contract);

      return xmlDoc;
    }

    private void AddConstructorNode(ref XmlDocument xmlDoc, string name, string pre, string post, Dictionary<string, Type> parameters)
    {
      XmlNode constructor = xmlDoc.CreateElement("constructor");
      XmlAttribute attr = xmlDoc.CreateAttribute("name");
      attr.Value = name;
      constructor.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("pre");
      attr.Value = GetValueFromCondition(pre);
      constructor.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("post");
      attr.Value = GetValueFromCondition(post);
      constructor.Attributes.Append(attr);

      if (parameters != null)
      {
        foreach (var param in parameters)
        {
          XmlNode parameter = xmlDoc.CreateElement("parameter");
          attr = xmlDoc.CreateAttribute("name");
          attr.Value = param.Key;
          parameter.Attributes.Append(attr);
          attr = xmlDoc.CreateAttribute("type");
          attr.Value = TypeToString(param.Value);
          parameter.Attributes.Append(attr);
          constructor.AppendChild(parameter);
        }
      }

      xmlDoc["contract"].AppendChild(constructor);
    }

    private void AddMethodNode(ref XmlDocument xmlDoc, string name, string pre, string post, Dictionary<string, Type> parameters)
    {
      XmlNode method = xmlDoc.CreateElement("action");
      XmlAttribute attr = xmlDoc.CreateAttribute("name");
      attr.Value = name;
      method.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("pre");
      attr.Value = GetValueFromCondition(pre);
      method.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("post");
      attr.Value = GetValueFromCondition(post);
      method.Attributes.Append(attr);

      if (parameters != null)
      {
        foreach (var param in parameters)
        {
          XmlNode parameter = xmlDoc.CreateElement("parameter");
          attr = xmlDoc.CreateAttribute("name");
          attr.Value = param.Key;
          parameter.Attributes.Append(attr);
          attr = xmlDoc.CreateAttribute("type");
          attr.Value = TypeToString(param.Value);
          parameter.Attributes.Append(attr);
          method.AppendChild(parameter);
        }
      }

      xmlDoc["contract"].AppendChild(method);
    }

    private void AddVariableNode(ref XmlDocument xmlDoc, string name, Type type)
    {
      XmlNode variable = xmlDoc.CreateElement("variable");
      XmlAttribute attr = xmlDoc.CreateAttribute("name");
      attr.Value = name;
      variable.Attributes.Append(attr);
      attr = xmlDoc.CreateAttribute("type");
      attr.Value = TypeToString(type);
      variable.Attributes.Append(attr);
      xmlDoc["contract"].AppendChild(variable);
    }

    private string GetValueFromCondition(string condition)
    {
      return (string.IsNullOrEmpty(condition) ? "TRUE" : condition.Replace("RET_Boolean=", ""));
    }

    private string TypeToString(Type type)
    {
      //Probably is the class being analyzed.
      if (type == null)
      {
        Logger.Warn(string.Format("NULL type!"));
        return "INT";
      }

      switch (type.Name.ToLower())
      {
        case "boolean":
          return "BOOLEAN";

        case "int8":
        case "int16":
        case "int32":
        case "int64":
        case "uint8":
        case "uint16":
        case "uint32":
        case "uint64":
          return "INT";

        case "decimal":
        //0.0M
        case "double":
        //0.0D
        case "float":
          //0.0F
          return "REAL";

        case "string":
        case "char":
        //'\0'
        case "struct":
        case "enum":
        default:
          Logger.Warn(string.Format("Unknown type: {0}. UnderlyingSystemType: {1}", type.Name, type.UnderlyingSystemType.Name));
          //dummy variable
          return type.Name.ToLower().Contains("int") ? "INT" : "BOOLEAN";
      }
    }

    public XmlDocument GetXML()
    {
      XmlDocument xmlDoc = CreateContract(name, invariant);

      foreach (var variable in variables)
        AddVariableNode(ref xmlDoc, variable.Key, variable.Value);

      foreach (var c in constructors)
        foreach (var ctor in c.Value)
          AddConstructorNode(ref xmlDoc, ctor.name, ctor.pre, ctor.post, ctor.parameters);

      foreach (var m in methods)
        foreach (var method in m.Value)
          AddMethodNode(ref xmlDoc, method.name, method.pre, method.post, method.parameters);

      return xmlDoc;
    }
  }
}