﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Cci;

namespace Translator
{
  public enum ExprType { INT, BOOL, ARRAY_INT, ARRAY_BOOL, UNKNOWN };

  // Expr models a valid expression in the language.
  internal class Expr
  {
    #region Fields

    private string value_;
    private ExprType type_;

    #endregion Fields

    #region Properties

    public string Value
    {
      get
      {
        if (string.IsNullOrEmpty(value_) && type_ != ExprType.UNKNOWN)
          throw new Exception("How did I get here? This doesn't have to happen!");
        return value_;
      }
    }

    public ExprType Type { get { return type_; } }

    #endregion Properties

    public Expr(string value, ExprType type)
    {
      value_ = value;
      type_ = type;

      if (string.IsNullOrEmpty(value) && type != ExprType.UNKNOWN)
        throw new ArgumentException("Invalid expression!");
    }

    public Expr(string value, PrimitiveTypeCode primitive_type)
    {
      value_ = value;
      type_ = Expr.GetType(primitive_type);

      if (string.IsNullOrEmpty(value_) && type_ != ExprType.UNKNOWN)
        throw new ArgumentException("Invalid expression!");
    }

    public static Expr Join(string format, ExprType new_type, params Expr[] exps)
    {
      if (exps.Length == 0)
        throw new ArgumentException("Expr Join without parameters!");

      ExprType type = exps.First().type_;
      // If the types don't match then the translation is not precise enough. :( (TODO: or the instr uses different
      // types)
      foreach (var exp in exps)
      {
        if (exp.type_ != type)
        {
          type = ExprType.UNKNOWN;
          break;
        }
      }
      if (type == ExprType.UNKNOWN)
        return new Expr(string.Empty, ExprType.UNKNOWN);

      // All the expressions have the same type, we are safe.
      string[] expsValues = new string[exps.Length];
      for (int i = 0; i < exps.Length; i++)
        expsValues[i] = exps[i].value_;

      return new Expr(string.Format(format, expsValues), new_type);
    }

    public override string ToString()
    {
      return value_;
    }

    public static Expr DefaultValue(PrimitiveTypeCode typeCode)
    {
      ExprType type = GetType(typeCode);
      switch (type)
      {
        case ExprType.INT:
          return new Expr("0", ExprType.INT);

        case ExprType.BOOL:
          return new Expr("FALSE", ExprType.BOOL);

        case ExprType.ARRAY_INT:
          return new Expr(string.Empty, ExprType.ARRAY_INT);

        case ExprType.ARRAY_BOOL:
          return new Expr(string.Empty, ExprType.ARRAY_BOOL);

        case ExprType.UNKNOWN:
          return new Expr(string.Empty, ExprType.UNKNOWN);

        default:
          throw new NotSupportedException("Missing type");
      }
    }

    public static ExprType GetType(PrimitiveTypeCode typeCode)
    {
      switch (typeCode)
      {
        case PrimitiveTypeCode.Boolean:
          return ExprType.BOOL;

        case PrimitiveTypeCode.Int8:
        case PrimitiveTypeCode.Int16:
        case PrimitiveTypeCode.Int32:
        case PrimitiveTypeCode.Int64:
        case PrimitiveTypeCode.UInt8:
        case PrimitiveTypeCode.UInt16:
        case PrimitiveTypeCode.UInt32:
        case PrimitiveTypeCode.UInt64:
        case PrimitiveTypeCode.Float32:
        case PrimitiveTypeCode.Float64:
          return ExprType.INT;

        case PrimitiveTypeCode.Char:
        case PrimitiveTypeCode.IntPtr:
        case PrimitiveTypeCode.Invalid:
        case PrimitiveTypeCode.NotPrimitive:
        case PrimitiveTypeCode.Pointer:
        case PrimitiveTypeCode.Reference:
        case PrimitiveTypeCode.String:
        case PrimitiveTypeCode.UIntPtr:
        case PrimitiveTypeCode.Void:
        default:
          break;
      }

      throw new NotSupportedException(string.Format("Unknown variable type: {0}.", typeCode));
    }
  }
}