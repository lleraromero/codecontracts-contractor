﻿using System;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Reflection;

namespace Translator
{
  public enum LogLevel { ERROR, WARN, INFO };

  internal class Logger
  {
    private static StringBuilder log_;

    public static void Log(LogLevel level, string msg)
    {
      if (log_ == null)
        log_ = new StringBuilder();

      log_.AppendFormat("{3} - {0}: {1}\n{2}\n", level.ToString(), msg, GetStackTraceWithMethods(), DateTime.Now);
    }

    private static string GetStackTraceWithMethods()
    {
      StringBuilder result = new StringBuilder();
      StackTrace trace = new StackTrace(3, true);
      int frameCount = trace.FrameCount;
      for (int n = 0; n < frameCount; ++n)
      {
        StringBuilder frameString = new StringBuilder();
        StackFrame frame = trace.GetFrame(n);

        int lineNumber = frame.GetFileLineNumber();
        string fileName = frame.GetFileName();
        MethodBase methodBase = frame.GetMethod();
        StringBuilder methodName = new StringBuilder(methodBase.Name);
        ParameterInfo[] paramInfos = methodBase.GetParameters();
        if (string.IsNullOrEmpty(fileName))
          continue;

        if (paramInfos.Length == 0)
        {
          // No parameters for this method; display empty parentheses.
          methodName.Append("()");
        }
        else
        {
          // Iterate over parameters, displaying each parameter's type and name.
          methodName.Append("(");
          int count = paramInfos.Length;
          for (int i = 0; i < count; ++i)
          {
            Type paramType = paramInfos[i].ParameterType;
            methodName.AppendFormat("{0} {1}",
                                paramType.ToString(),
                                paramInfos[i].Name);
            if (i < count - 1)
              methodName.Append(",");
          }
          methodName.Append(")");
        }

        result.AppendFormat("  at {0} in {1}:{2}\n",
                          methodBase.ReflectedType.FullName + "." + methodName.ToString(),
                          fileName,
                          lineNumber);
      }
      return result.ToString();
    }

    public static void Error(string msg)
    {
      Log(LogLevel.ERROR, msg);
    }

    public static void Warn(string msg)
    {
      Log(LogLevel.WARN, msg);
    }

    public static void Info(string msg)
    {
      Log(LogLevel.INFO, msg);
    }

    public static new string ToString()
    {
      return (log_ != null) ? log_.ToString() : string.Empty;
    }
  }
}