﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Microsoft.Cci;
using Microsoft.Cci.Contracts;
using Microsoft.Cci.ILToCodeModel;
using Microsoft.Cci.MutableContracts;

namespace Translator
{
  public class ContractDecompiler
  {
    public class DecompilationResult
    {
      public string output { get; set; }

      public XmlDocument document { get; set; }
    }

    private CodeContractAwareHostEnvironment Host { get; set; }

    private ContractProvider ContractProvider { get; set; }

    private Dictionary<string, object> Vars { get; set; }

    private Dictionary<IMethodDefinition, string> PureMethods { get; set; }

    public DecompilationResult Decompile(FileInfo file, string selectedClass)
    {
      using (Host = new CodeContractAwareHostEnvironment())
      {
        // Read the Metadata Model from the PE file, if it is a valid module
        var module = LoadModuleOrDie(file.FullName, Host);

        // Get a PDB reader if there is a PDB file.
        PdbReader pdbReader = GetPDBReader(Host, module);

        using (pdbReader)
        {
          ISourceLocationProvider sourceLocationProvider = pdbReader;
          // Construct a Code Model from the Metadata model via decompilation
          var mutableModule = Decompiler.GetCodeModelFromMetadataModel(Host, module, pdbReader,
            DecompilerOptions.AnonymousDelegates | DecompilerOptions.Loops);
          ILocalScopeProvider localScopeProvider = new Decompiler.LocalScopeProvider(pdbReader);
          // Extract contracts (side effect: removes them from the method bodies)
          ContractProvider = ContractHelper.ExtractContracts(Host, mutableModule, pdbReader, localScopeProvider);

          // Get the type that matches with the selected class.
          INamedTypeDefinition type = mutableModule.AllTypes.Find(x => x.Name.Value == selectedClass);
          ITypeContract typeContract = ContractProvider.GetTypeContractFor(type);

          // Get the properties, or fields if there are no props.
          Vars = GetPropertiesOrFields(type);

          // Pre-translate pure methods postconditions since they can be used in pre or post of other methods.
          PureMethods = GetPureMethodsValues(type);

          // Translate the class invariant.
          string invariant = GetInvariant(typeContract);
          var translator = new ContractTranslator(type.Name.Value, invariant);

          // Traverse the class looking for public methods/constructors
          foreach (var method in type.Methods)
          {
            // If the method isn't public or it's a property there is no need to include it.
            if (method.Visibility != TypeMemberVisibility.Public || IsProperty(method.ResolvedMethod))
              continue;

            // Retrieve the contract from the method if it exists.
            var methodContract = ContractProvider.GetMethodContractFor(method);

            // Translate the method precondition
            string pre = GetPrecondition(methodContract);
            // and postcondition.
            string post = GetPostcondition(methodContract);

            // If the method has parameters they should be included as well.
            var parameters = new Dictionary<string, Type>();
            foreach (var param in method.Parameters)
              parameters.Add(param.Name.Value, Type.GetType(param.Type.ToString()) ?? typeof(object));

            // Add the method to the translation.
            if (method.IsConstructor)
              translator.addConstructor(method.Name.Value, pre, post, parameters);
            else
              translator.addMethod(method.Name.Value, pre, post, parameters);
          }

          // Lastly, add the variables (properties or fields) to the translation.
          foreach (var prop in Vars)
            translator.addVariable(GetVarName(prop), GetVarType(prop.Value));

          DecompilationResult result = new DecompilationResult()
          {
            document = translator.GetXML(),
            output = Logger.ToString()
          };

          return result;
        }
      }
    }

    private PdbReader GetPDBReader(CodeContractAwareHostEnvironment Host, IModule module)
    {
      PdbReader pdbReader = null;
      string pdbFile = Path.ChangeExtension(module.Location, "pdb");
      if (File.Exists(pdbFile))
        using (var pdbStream = File.OpenRead(pdbFile))
          pdbReader = new PdbReader(pdbStream, Host);
      return pdbReader;
    }

    private IModule LoadModuleOrDie(string fullName, CodeContractAwareHostEnvironment Host)
    {
      var module = Host.LoadUnitFrom(fullName) as IModule;
      if (module == null || module is Dummy)
        throw new Exception(fullName + " is not a PE file containing a CLR module or assembly.");
      return module;
    }

    /// <summary>
    /// Returns all the classes contained in the assembly.
    /// </summary>
    /// <param name="inputFileName">Path of the file</param>
    /// <param name="fileName">filename</param>
    /// <returns></returns>
    public List<string> GetClasses(string fullName)
    {
      List<string> classes = new List<string>();

      using (var Host = new CodeContractAwareHostEnvironment())
      {
        // Read the Metadata Model from the PE file
        var module = LoadModuleOrDie(fullName, Host);

        // Get a PDB reader if there is a PDB file.
        PdbReader pdbReader = GetPDBReader(Host, module);
        using (pdbReader)
        {
          ISourceLocationProvider sourceLocationProvider = pdbReader;
          // Construct a Code Model from the Metadata model via decompilation.
          var mutableModule = Decompiler.GetCodeModelFromMetadataModel(Host, module, pdbReader,
            DecompilerOptions.AnonymousDelegates | DecompilerOptions.Loops);

          // I want to show only those classes that are exported by the module.
          foreach (var type in mutableModule.AllTypes)
            if (TypeHelper.IsVisibleOutsideAssembly(type) && type.IsClass)
              classes.Add(type.Name.Value);
        }
      }
      classes.Sort();
      return classes;
    }

    private string TranslateContractElements(IEnumerable<IContractElement> elements)
    {
      StringBuilder translatedContract = new StringBuilder();
      var vars = Vars;
      var visitor = new CVC3Visitor(ref vars, PureMethods, Host, ContractProvider);
      Vars = vars;
      foreach (var elem in elements)
      {
        try
        {
          elem.Dispatch(visitor);
          if (translatedContract.Length != 0 && !string.IsNullOrEmpty(visitor.ToString()))
            translatedContract.Append(" AND ");
          translatedContract.Append(visitor);
        }
        catch (Exception e)
        {
          Logger.Error(e.Message);
        }
      }
      return translatedContract.ToString();
    }

    private string GetInvariant(ITypeContract typeContract)
    {
      if (typeContract != null)
        return TranslateContractElements(typeContract.Invariants);
      return string.Empty;
    }

    private string GetPrecondition(IMethodContract methodContract)
    {
      if (methodContract != null)
        return TranslateContractElements(methodContract.Preconditions);
      return string.Empty;
    }

    private string GetPostcondition(IMethodContract methodContract)
    {
      if (methodContract != null && !methodContract.IsPure)
        return TranslateContractElements(methodContract.Postconditions);
      return string.Empty;
    }

    private Dictionary<IMethodDefinition, string> GetPureMethodsValues(INamedTypeDefinition type)
    {
      var pureMethods = new Dictionary<IMethodDefinition, string>();
      foreach (var method in type.Methods)
      {
        var methodContract = ContractProvider.GetMethodContractFor(method);
        if (methodContract != null && methodContract.IsPure)
        {
          var result = TranslateContractElements(methodContract.Postconditions);
          pureMethods.Add(method, result);
        }
      }
      return pureMethods;
    }

    private string GetVarName(KeyValuePair<string, object> var)
    {
      object type = var.Value;
      string id = var.Key;
      //Property or field?
      if (type is IPropertyDefinition || type is IFieldDefinition) return ((INamedEntity)type).Name.Value;
      //Getter?
      else if (type is String) return ((string)type);

      return id;
    }

    private Dictionary<string, object> GetPropertiesOrFields(INamedTypeDefinition type)
    {
      var vars = new Dictionary<string, object>();

      // Properties are prefered since they are intended to describe the object.
      foreach (var prop in type.Properties)
        vars.Add(prop.Getter.ResolvedMethod.Name.Value, prop);

      // But if I couldn't find properties I will get the fields.
      if (vars.Count == 0)
        foreach (var field in type.Fields)
          vars.Add(field.Name.Value, field);

      return vars;
    }

    private Type GetVarType(object var)
    {
      if (var is IPropertyDefinition) return Type.GetType(((IPropertyDefinition)var).Type.ToString());
      else if (var is IFieldDefinition) return Type.GetType(((IFieldDefinition)var).Type.ToString());
      else if (var is Type) return (Type)var;

      return typeof(object);
    }

    private bool IsProperty(IMethodDefinition method)
    {
      return MemberHelper.IsGetter(method) || MemberHelper.IsSetter(method);
    }
  }
}