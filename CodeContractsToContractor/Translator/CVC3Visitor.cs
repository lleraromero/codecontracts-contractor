﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CSharpSourceEmitter;
using Microsoft.Cci;
using Microsoft.Cci.Contracts;
using Microsoft.Cci.MutableContracts;

namespace Translator
{
  public class CVC3Visitor : CodeAndContractVisitor
  {
    private CodeContractAwareHostEnvironment host;
    private bool isPostcondition;
    private Stack<Expr> parameters;
    private Dictionary<IMethodDefinition, string> pureMethods;
    private string toString;
    private Dictionary<string, object> variables;

    /// <summary>
    /// Contains a specialized Visit routine for each standard type of object defined in the contract, code and metadata
    /// model.
    /// </summary>
    public CVC3Visitor(ref Dictionary<string, object> variables, Dictionary<IMethodDefinition, string> pureMethods, CodeContractAwareHostEnvironment host, IContractProvider contractProvider)
      : base(contractProvider)
    {
      this.variables = variables;
      this.pureMethods = pureMethods;
      this.host = host;
      parameters = new Stack<Expr>();

      this.isPostcondition = false;
    }

    public override string ToString()
    {
      if (parameters.Count > 0)
        toString = RemoveRedundantParantheses(parameters.Pop().Value);

      return toString;
    }

    #region InstructionsCategories

    public override void Visit(IExpression expr)
    {
      Logger.Warn(string.Format("Instruction \"{0}\" is not implemented. Type: {1}", PrintExpression(expr), expr.GetType()));
      parameters.Push(new Expr("", ExprType.UNKNOWN));
    }

    public override void Visit(IStatement stmt)
    {
      Logger.Warn(string.Format("Instruction \"{0}\" is not implemented. Type: {1}", PrintStatement(stmt), stmt.GetType()));
      parameters.Push(new Expr("", ExprType.UNKNOWN));
    }

    public override void Visit(IBinaryOperation binaryOperation)
    {
      binaryOperation.RightOperand.Dispatch(this);
      binaryOperation.LeftOperand.Dispatch(this);
    }

    public override void Visit(IUnaryOperation unaryOperation)
    {
      unaryOperation.Operand.Dispatch(this);
    }

    #endregion InstructionsCategories

    #region ContractVisitor

    public override void Visit(IContractElement contractElement)
    {
    }

    public override void Visit(ILoopContract loopContract)
    {
    }

    public override void Visit(IMethodContract methodContract)
    {
    }

    public override void Visit(IThrownException thrownException)
    {
    }

    public override void Visit(ITypeContract typeContract)
    {
    }

    #endregion ContractVisitor

    #region Helpers

    private string RemoveRedundantParantheses(string oldString)
    {
      string newString = oldString;
      int length = -1;
      while (length != newString.Length)
      {
        length = newString.Length;
        newString = Regex.Replace(oldString, @"\(\(([^()]*)\)\)", @"(${1})");
      }
      return newString;
    }

    private bool IsVariable(string candidate)
    {
      return !string.IsNullOrEmpty(TryParseVariable(candidate));
    }

    private string PrintExpression(IExpression expr)
    {
      var sourceEmitterOutput = new SourceEmitterOutputString();
      var CSSourceEmitter = new CSharpSourceEmitter.SourceEmitter(sourceEmitterOutput);
      CSSourceEmitter.Traverse(expr);
      return sourceEmitterOutput.Data;
    }

    private string PrintStatement(IStatement stmt)
    {
      var sourceEmitterOutput = new SourceEmitterOutputString();
      var CSSourceEmitter = new CSharpSourceEmitter.SourceEmitter(sourceEmitterOutput);
      CSSourceEmitter.Traverse(stmt);
      return sourceEmitterOutput.Data;
    }

    private string TryParseVariable(string candidate)
    {
      if (!variables.ContainsKey(candidate))
        return null;
      object var = variables[candidate];
      if (var is IPropertyDefinition)
        return ((IPropertyDefinition)var).Name.Value;
      else if (var is IFieldDefinition)
        return ((IFieldDefinition)var).Name.Value;

      throw new MissingFieldException("Unknown variable definition.");
    }

    #endregion Helpers

    private void TransformExpr(IExpression instr, string value, ExprType type = ExprType.UNKNOWN)
    {
      parameters.Push(new Expr(value, type));
    }

    private void TransformExpr(IStatement instr, string value, ExprType type = ExprType.UNKNOWN)
    {
      parameters.Push(new Expr(value, type));
    }

    private void TransformExpr(IUnaryOperation instr, string format, ExprType type)
    {
      TransformExpr(1, format, type);
    }

    private void TransformExpr(IBinaryOperation instr, string format, ExprType type)
    {
      TransformExpr(2, format, type);
    }

    private void TransformExpr(int paramCount, string format, ExprType type)
    {
      List<Expr> exprs = new List<Expr>();
      for (int i = 0; i < paramCount; i++)
        exprs.Add(parameters.Pop());

      parameters.Push(Expr.Join(format, type, exprs.ToArray()));
    }

    #region Miembros de ICodeAndContractVisitor

    public override void Visit(ITypeInvariant typeInvariant)
    {
      typeInvariant.Condition.Dispatch(this);
    }

    public override void Visit(IPrecondition precondition)
    {
      precondition.Condition.Dispatch(this);
    }

    public override void Visit(IPostcondition postCondition)
    {
      isPostcondition = true;
      postCondition.Condition.Dispatch(this);
    }

    #endregion Miembros de ICodeAndContractVisitor

    #region Miembros de ICodeVisitor

    public override void Visit(IAnonymousDelegate expr)
    {
      foreach (var stmt in expr.Body.Statements)
      {
        if (stmt is IReturnStatement)
        {
          var retStmt = stmt as IReturnStatement;
          if (retStmt != null && retStmt.Expression != null)
          {
            foreach (var param in expr.Parameters)
              param.Dispatch(this);
            retStmt.Expression.Dispatch(this);
          }
        }
        else if (stmt is IExpressionStatement)
        {
          var exprStmt = stmt as IExpressionStatement;
          if (exprStmt != null && expr.ReturnType.TypeCode == PrimitiveTypeCode.Void)
          {
            foreach (var param in expr.Parameters)
              param.Dispatch(this);
            exprStmt.Expression.Dispatch(this);
          }
        }
      }
    }

    public override void Visit(IVectorLength instr)
    {
      instr.Vector.Dispatch(this);
      string lastVariable = parameters.Pop().Value;
      //ToDo: revisar el tipo
      if (!variables.ContainsKey(lastVariable + "_Length"))
        variables.Add(lastVariable + "_Length", typeof(int)); //PrimitiveTypeCode.Int32
      parameters.Push(new Expr(lastVariable + "_Length", ExprType.INT));
    }

    public override void Visit(IUnaryPlus instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}+1)", ExprType.INT);
    }

    public override void Visit(IUnaryNegation instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "NOT({0})", ExprType.BOOL);
    }

    public override void Visit(ISubtraction instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}-{1})", ExprType.INT);
    }

    public override void Visit(IReturnValue instr)
    {
      //TODO: me interesa el RET solo si estoy mirando los metodos puros
      //parameters.Push(new Expr("RET", instr.Type.TypeCode));
      parameters.Push(new Expr("", ExprType.UNKNOWN));
    }

    public override void Visit(IOldValue oldValue)
    {
      bool savePostValue = isPostcondition;
      isPostcondition = false;
      oldValue.Expression.Dispatch(this);
      isPostcondition = savePostValue;
    }

    public override void Visit(INotEquality instr)
    {
      base.Visit(instr);
      // Pure methods can add extra information for its result that need to be included.
      if (instr.LeftOperand is IReturnValue)
      {
        TransformExpr(instr, "NOT({1})", ExprType.BOOL);
        return;
      }
      else if (instr.RightOperand is IReturnValue)
      {
        TransformExpr(instr, "NOT({0})", ExprType.BOOL);
        return;
      }

      TransformExpr(instr, "NOT({0}={1})", ExprType.BOOL);
    }

    public override void Visit(IMultiplication instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}*{1})", ExprType.INT);
    }

    public override void Visit(IModulus instr)
    {
      /*
      ToDo: Modulo operator - Add more theory?
          IDIV: (INT, INT) -> INT;
          ASSERT (FORALL (x, y, z: INT) : (NOT y = 0) => (IDIV(x, y) = z <=> y * z <= x AND y * (z + 1) > x));
          IMOD: (INT, INT) -> INT = LAMBDA (x: INT, y: INT): x - IDIV(x, y) * y;
      */
      base.Visit(instr);
      TransformExpr(instr, "({0} mod {1})", ExprType.INT);
    }

    public override void Visit(IMethodCall methodCall)
    {
      foreach (var param in methodCall.Arguments)
        param.Dispatch(this);

      methodCall.MethodToCall.Dispatch(this);
    }

    public override void Visit(ILogicalNot instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "NOT({0})", ExprType.BOOL);
    }

    public override void Visit(ILessThanOrEqual instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}<={1})", ExprType.BOOL);
    }

    public override void Visit(ILessThan instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}<{1})", ExprType.BOOL);
    }

    public override void Visit(IGreaterThanOrEqual instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}>={1})", ExprType.BOOL);
    }

    public override void Visit(IGreaterThan instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}>{1})", ExprType.BOOL);
    }

    public override void Visit(IEquality instr)
    {
      base.Visit(instr);
      // Pure methods can add extra information for its result that need to be included.
      if (instr.LeftOperand is IReturnValue)
      {
        TransformExpr(instr, "{1}", ExprType.BOOL);
        return;
      }
      else if (instr.RightOperand is IReturnValue)
      {
        TransformExpr(instr, "{0}", ExprType.BOOL);
        return;
      }

      TransformExpr(instr, "({0}={1})", ExprType.BOOL);
    }

    public override void Visit(IDivision instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}/{1})", ExprType.INT);
    }

    public override void Visit(IDefaultValue defaultValue)
    {
      parameters.Push(Expr.DefaultValue(defaultValue.Type.TypeCode));
    }

    public override void Visit(IConditional instr)
    {
      instr.Condition.Dispatch(this);
      //Condition || Something
      if (instr.ResultIfTrue.ToString().Equals("true", System.StringComparison.InvariantCultureIgnoreCase))
      {
        instr.ResultIfFalse.Dispatch(this);
        TransformExpr(2, "({0} OR {1})", ExprType.BOOL);
      }
      //!(Condition) && Something
      else if (instr.ResultIfTrue.ToString().Equals("false", System.StringComparison.InvariantCultureIgnoreCase))
      {
        instr.ResultIfFalse.Dispatch(this);
        TransformExpr(2, "({0} AND NOT({1}))", ExprType.BOOL);
      }
      //Condition && Something
      else if (instr.ResultIfFalse.ToString().Equals("false", System.StringComparison.InvariantCultureIgnoreCase))
      {
        instr.ResultIfTrue.Dispatch(this);
        TransformExpr(2, "({0} AND {1})", ExprType.BOOL);
      }
      //!(Condition) || Something
      else if (instr.ResultIfFalse.ToString().Equals("true", System.StringComparison.InvariantCultureIgnoreCase))
      {
        instr.ResultIfTrue.Dispatch(this);
        TransformExpr(2, "({0} OR NOT({1}))", ExprType.BOOL);
      }
    }

    public override void Visit(IConversion instr)
    {
      instr.ValueToConvert.Dispatch(this);
    }

    public override void Visit(ICompileTimeConstant constant)
    {
      if (ExpressionHelper.IsNullLiteral(constant))
      {
        parameters.Push(new Expr("0", ExprType.INT));
        return;
      }

      ExprType type = Expr.GetType(constant.Type.TypeCode);
      switch (type)
      {
        case ExprType.INT:
          parameters.Push(new Expr(int.Parse(constant.Value.ToString()).ToString(), ExprType.INT));
          break;

        case ExprType.BOOL:
          parameters.Push(new Expr(bool.Parse(constant.Value.ToString()).ToString().ToUpper(), ExprType.BOOL));
          break;

        case ExprType.ARRAY_INT:
        case ExprType.ARRAY_BOOL:
        case ExprType.UNKNOWN:
        default:
          Logger.Warn(string.Format("Unknown value: {0}", constant.Value.ToString()));
          parameters.Push(new Expr(constant.Value.ToString(), ExprType.UNKNOWN));
          break;
      }
    }

    public override void Visit(IBoundExpression instr)
    {
      var def = instr.Definition;
      if (def is IFieldDefinition)
        ((IFieldDefinition)def).Dispatch(this);
      else if (def is IFieldReference)
        ((IFieldReference)def).Dispatch(this);
      else if (def is IParameterDefinition)
        ((IParameterDefinition)def).Dispatch(this);
      else
        this.Visit((IExpression)instr);
    }

    public override void Visit(IBlockExpression instr)
    {
      instr.Expression.Dispatch(this);
    }

    public override void Visit(IAddition instr)
    {
      base.Visit(instr);
      TransformExpr(instr, "({0}+{1})", ExprType.INT);
    }

    #endregion Miembros de ICodeVisitor

    #region Miembros de IMetadataVisitor

    public override void Visit(IParameterDefinition instr)
    {
      parameters.Push(new Expr(instr.Name.Value, instr.Type.TypeCode));
    }

    public override void Visit(IMethodReference instr)
    {
      if (MemberHelper.IsGetter(instr.ResolvedMethod))
      {
        string candidate = TryParseVariable(instr.ResolvedMethod.Name.Value);
        if (isPostcondition)
          candidate += "'";
        parameters.Push(new Expr(candidate, instr.ResolvedMethod.Type.TypeCode));
      }
      else if (instr.Name.Equals(host.NameTable.GetNameFor("ForAll")))
      {
        Expr condition = parameters.Pop();
        Expr variable = parameters.Pop();
        Expr upperBound = parameters.Pop();
        Expr lowerBound = parameters.Pop();
        // TODO: will fail in the join due to different types
        parameters.Push(Expr.Join("(FORALL ({0}:INT): {0} >= {1} AND {0} < {2} AND {3})", ExprType.BOOL, variable, lowerBound, upperBound, condition));
      }
      else if (instr.Name.Equals(host.NameTable.GetNameFor("Exists")))
      {
        var condition = parameters.Pop();
        var variable = parameters.Pop();
        parameters.Push(Expr.Join("(EXISTS ({0}: INT): {1})", ExprType.INT, variable, condition));
      }
      //Some random method.
      else
      {
        var methodSig = MemberHelper.GetMethodSignature(instr, NameFormattingOptions.Signature | NameFormattingOptions.TypeParameters | NameFormattingOptions.ParameterModifiers | NameFormattingOptions.ReturnType);
        Logger.Warn(string.Format("Method: {0}", methodSig));
      }
    }

    public override void Visit(IMethodDefinition instr)
    {
      if (MemberHelper.IsGetter(instr))
        parameters.Push(new Expr(TryParseVariable(instr.Name.Value) + (isPostcondition ? "'" : ""), instr.ResolvedMethod.Type.TypeCode));
      else
      {
        if (pureMethods.ContainsKey(instr))
        {
          parameters.Push(new Expr(pureMethods[instr] + (isPostcondition ? "'" : ""), ExprType.BOOL));
        }
        else
        {
          var methodSig = MemberHelper.GetMethodSignature(instr, NameFormattingOptions.Signature | NameFormattingOptions.TypeParameters | NameFormattingOptions.ParameterModifiers | NameFormattingOptions.ReturnType);
          Logger.Warn(string.Format("Method: {0}", methodSig));
        }
      }
    }

    public override void Visit(IFieldDefinition instr)
    {
      string candidate = TryParseVariable(instr.Name.Value);
      if (!string.IsNullOrEmpty(candidate))
      {
        if (isPostcondition)
          candidate += "'";
        parameters.Push(new Expr(candidate, instr.Type.TypeCode));
      }
      else
      {
        Logger.Warn(string.Format("FieldDefinition: {0}", instr.Name.Value));
        this.Visit((IExpression)instr);
      }
    }

    public override void Visit(IFieldReference instr)
    {
      string candidate = TryParseVariable(instr.Name.Value);
      if (!string.IsNullOrEmpty(candidate))
      {
        if (isPostcondition)
          candidate += "'";
        parameters.Push(new Expr(candidate, instr.Type.TypeCode));
      }
      else
      {
        Logger.Warn(string.Format("FieldReference: {0}", instr.Name.Value));
        this.Visit((IExpression)instr);
      }
    }

    #endregion Miembros de IMetadataVisitor
  }
}