﻿using Mono.Unix;
using Mono.Unix.Native;
using Service;
using System;
using System.Configuration;

namespace LinuxService
{
    /*  /etc/init/contractor.conf
        # Contractor Translator

        description "Contractor Translator"
        author      "lleraromero"

        start on started rc
        stop on stopping rc

        respawn

        exec start-stop-daemon --start -c username --exec mono /path/to/application.exe
     */
    class Program
    {
        private static string ListeningOn { get { return string.Format("http://+:{0}/", ConfigurationManager.AppSettings["ServerPort"]); } }

        private static void Main(string[] args)
        {
            var appHost = new ContractorAppHost();
            appHost.Init();
            appHost.Start(ListeningOn);
            System.Console.WriteLine("AppHost Created at {0}, listening on {1}", DateTime.Now, ListeningOn);

            UnixSignal[] signals = new UnixSignal[] { 
                new UnixSignal(Signum.SIGINT), 
                new UnixSignal(Signum.SIGTERM), 
            };

            // Wait for a unix signal
            for (bool exit = false; !exit; )
            {
                int id = UnixSignal.WaitAny(signals);

                if (id >= 0 && id < signals.Length)
                {
                    if (signals[id].IsSet) exit = true;
                }
            }
        }
    }
}