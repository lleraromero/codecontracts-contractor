﻿using System.IO;
using System.Net;

namespace Tests
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      //TODO: agregar proyecto de testing con archivos de control para ir probando que instrucciones falta soportar
      //ProcessClass("ATM");
      //ProcessClass("BoundedStack");
      //ProcessClass("Door");
      //ProcessClass("EclatStack");
      //ProcessClass("FiniteStack");
      ProcessClass("Linear");
      //ProcessClass("VendingMachine");
    }

    private static void ProcessClass(string c)
    {
      HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://localhost:4635/files/examples-lafhis/" + c);
      request.Method = "GET";
      request.Timeout = int.MaxValue;

      using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
      using (Stream dataStream = response.GetResponseStream())
      using (StreamReader reader = new StreamReader(dataStream))
      {
        reader.ReadToEnd();
      }
    }
  }
}