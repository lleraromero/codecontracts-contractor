﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Service
{
  [Route("/api/status")]
  public class Monitor
  {
  }

  public class MonitorResponse : IHasResponseStatus
  {
    /// <summary>
    /// Gets or sets the ResponseStatus. The built-in IoC used with ServiceStack autowires this property.
    /// </summary>
    public ResponseStatus ResponseStatus { get; set; }

    public string Version { get; set; }
  }
}