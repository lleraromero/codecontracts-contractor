﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Service
{
  [Api("GET the File or Directory info at {Path}\n"
      + "POST multipart/formdata to upload a new file to any {Path} in the /ReadWrite folder\n")]
  [Route("/api/files")]
  [Route("/api/files/{Path}")]
  [Route("/api/files/{Path}/{SelectedClass}")]
  public class Files
  {
    public string Path { get; set; }

    public string SelectedClass { get; set; }
  }

  [Route("/api/files/status/{File}")]
  public class FileStatus
  {
    public string File { get; set; }
  }

  public class FilesResponse : IHasResponseStatus
  {
    public FileResult File { get; set; }

    /// <summary>
    /// Gets or sets the ResponseStatus. The built-in IoC used with ServiceStack autowires this property.
    /// </summary>
    public ResponseStatus ResponseStatus { get; set; }
  }

  public delegate void ProcessRequest(Files file);

  public class FileRequest
  {
    public Files File { get; set; }
  }
}