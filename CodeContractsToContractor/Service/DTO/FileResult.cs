using System.Collections.Generic;

namespace Service
{
  public class FileResult
  {
    public List<string> Classes { get; set; }

    public string Extension { get; set; }

    public long FileSizeBytes { get; set; }

    public string Name { get; set; }
  }
}