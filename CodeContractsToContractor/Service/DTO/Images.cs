﻿using System;
using ServiceStack.ServiceHost;

namespace Service
{
    [Route("/images/{Filename}")]
    public class Images { 
        public string Filename { get; set; }
    }
}

