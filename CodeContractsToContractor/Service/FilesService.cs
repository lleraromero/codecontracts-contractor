﻿using NLog;
using ServiceStack.Common.Web;
using ServiceStack.Redis.Messaging;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using Translator;
using File = System.IO.File;

namespace Service
{
    public class FilesService : ServiceStack.ServiceInterface.Service
    {
        public AppConfig Config { get; set; }

        public RedisMqServer MQServer { get; set; }
        // If there is no MQServer then I need the following structures to store the files
        private static Dictionary<string, FileRequest> requestedFiles;
        private static Dictionary<string, FileRequest> RequestedFiles
        {
            get
            {
                if (requestedFiles == null)
                    requestedFiles = new Dictionary<string, FileRequest>();
                return requestedFiles;
            }
        }
        private static HashSet<string> processedFiles;
        private static HashSet<string> ProcessedFiles
        {
            get
            {
                if (processedFiles == null)
                    processedFiles = new HashSet<string>();
                return processedFiles;
            }
        }

        private string ServiceUrl { get { return string.Format("http://localhost:{0}", Config.ServerPort); } }

        private const string EXAMPLE_FILE = "examples-lafhis";

        public void Get(FileStatus request)
        {
            if (MQServer == null && !ProcessedFiles.Contains(request.File))
            {
                // Single thread mode
                ProcessedFiles.Add(request.File);
                ProcessFile(RequestedFiles[request.File.Replace(".svg", "")].File);
            }
            var target = GetPath(request);
            var isExistingFile = target.Exists && (target.Attributes & FileAttributes.Directory) != FileAttributes.Directory;
            Response.StatusCode = isExistingFile ? 200 : 404;
        }

        public Dictionary<string, string> Get(Files request)
        {
            var target = GetPath(request);
            var isExistingFile = target.Exists && (target.Attributes & FileAttributes.Directory) != FileAttributes.Directory;
            if (!isExistingFile)
                throw new NotSupportedException("Could not find the requested file");

            LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "File requested: " + target.Name + "-" + request.SelectedClass);
            if (request.Path == EXAMPLE_FILE)
            {
                try
                {
                    File.Delete(target.FullName + "-" + request.SelectedClass + ".xml");
                    File.Delete(target.FullName + "-" + request.SelectedClass + ".gv");
                    File.Delete(AppConfig.GetAbsolutePath(Config.Images) + request.Path + "-" + request.SelectedClass + ".svg");
                }
                catch
                {
                }
            }

            // Enqueue the file to process
            if (MQServer != null)
            {
                var mqClient = MQServer.MessageFactory.CreateMessageQueueClient();
                mqClient.Publish(new FileRequest { File = request });
            }
            else
            {
                var key = string.Format("{0}-{1}", request.Path, request.SelectedClass);
                if (!RequestedFiles.ContainsKey(key))
                {
                    RequestedFiles.Add(key, new FileRequest { File = request });
                }
            }

            // This is the file that the frontend will have to request
            var filename = target.Name;
            var response = new Dictionary<string, string>();
            response.Add("filename", ServiceUrl + VirtualPathUtility.ToAbsolute(
                VirtualPathUtility.ToAbsolute(Config.Images + filename + "-" + request.SelectedClass + ".svg",
                    "/")));
            return response;
        }

        public FilesResponse Post(Files request)
        {
            var targetDir = GetPath(request);

            // Is the example file?
            if (!string.IsNullOrEmpty(request.Path) && request.Path == EXAMPLE_FILE)
            {
                try
                {
                    var response = new FilesResponse();
                    var example_file = targetDir.FullName;
                    response.File = GetFileResult(new FileInfo(example_file));
                    response.File.Classes = new ContractDecompiler().GetClasses(example_file);
                    return response;
                }
                catch (Exception e)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(e);
                }
                return null;
            }

            var isExistingFile = targetDir.Exists && (targetDir.Attributes & FileAttributes.Directory) != FileAttributes.Directory;
            if (isExistingFile)
                throw new NotSupportedException("POST only supports uploading new files.");

            if (!Directory.Exists(targetDir.FullName))
                Directory.CreateDirectory(targetDir.FullName);

            foreach (var uploadedFile in base.RequestContext.Files)
            {
                var newFilePath = string.Empty;
                var filename = string.Empty;
                do
                {
                    filename = RandomString();
                    newFilePath = Path.Combine(targetDir.FullName, filename);
                } while (File.Exists(newFilePath));
                uploadedFile.SaveTo(newFilePath);

                var response = new FilesResponse();
                response.File = GetFileResult(new FileInfo(newFilePath));
                response.File.Classes = new ContractDecompiler().GetClasses(newFilePath);

                return response;
            }

            throw new FileNotFoundException("No file found!");
        }

        public HttpResult Get(Images request)
        {
            return new HttpResult(new FileInfo(AppConfig.GetAbsolutePath(Path.Combine(Config.Images, request.Filename))), asAttachment: false) { ContentType = "image/svg+xml" };
        }

        private void ProcessFile(Files file)
        {
            var target = GetPath(file);

            // Transform the Contract Reference Assembly into a Contractor-like contract.
            ContractDecompiler.DecompilationResult result = new ContractDecompiler().Decompile(target, file.SelectedClass);

            // Any message from the decompiler?
            if (!string.IsNullOrEmpty(result.output))
                LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Trace, result.output);

            XmlDocument contract = result.document;
            var fullName = target.FullName;
            var filename = target.Name;

            // Save the contract so it can be transformed into a FSM.
            string path = string.Concat(Path.Combine(Config.WorkingDirectoryPath, filename), "-",
                file.SelectedClass, ".xml");
            contract.Save(path);

            try
            {
                // Transform the contract into a TypeState.
                GetTypeState(filename + "-" + file.SelectedClass);
            }
            catch (Exception e)
            {
                // There were errors in the translation. Instead, showing the trivial one.
                LogManager.GetCurrentClassLogger().Error(e);
                try
                {
                    contract = RollbackTranslation(contract);
                    contract.Save(path);
                    GetTypeState(filename + "-" + file.SelectedClass);
                }
                catch (Exception ex)
                {
                    // Errors in the rollback? Damn!
                    LogManager.GetCurrentClassLogger().Error(ex);
                    throw;
                }
            }
        }

        private static XmlDocument RollbackTranslation(XmlDocument contract)
        {
            Queue<XmlNode> left = new Queue<XmlNode>();
            left.Enqueue(contract.FirstChild);
            while (left.Count > 0)
            {
                XmlNode current = left.Dequeue();

                foreach (XmlAttribute attr in current.Attributes)
                {
                    if (attr.Name == "pre" || attr.Name == "post" || attr.Name == "invariant")
                        attr.Value = "TRUE";
                }

                foreach (XmlNode child in current.ChildNodes)
                {
                    left.Enqueue(child);
                }
            }
            return contract;
        }

        public void GetTypeState(string filename)
        {
            RunContractor(filename);
            RunDot(filename);
        }

        private void RunContractor(string filename)
        {
#if WIN
            string currentPath = Environment.GetEnvironmentVariable("path");
            if (!currentPath.Contains(Path.GetDirectoryName(Config.CVC3Path)))
            {
                Environment.SetEnvironmentVariable("path", string.Format("{0};{1};{2}", currentPath, Path.GetDirectoryName(Config.CVC3Path), Path.GetDirectoryName(Config.DotPath)));
            }
#endif

#if WIN
            string FLAGS = string.Format("--cvc3-path=\"{0}\"", Path.GetFileName(Config.CVC3Path));
#else
            string FLAGS = string.Format("--cvc3-path=\"{0}\"", Config.CVC3Path);
#endif

            //Transform the xml to a graphviz file.
            var start = new ProcessStartInfo();
            start.FileName = Config.PythonPath;
            start.Arguments = string.Format("\"{0}\" \"{1}\" {2} -o graphviz -f \"{3}\"",
                Config.ContratorPath,
                Path.Combine(Config.WorkingDirectoryPath, filename + ".xml"),
                FLAGS,
                Path.Combine(Config.WorkingDirectoryPath, filename + ".gv"));
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;

            StringBuilder stdout = new StringBuilder();
            StringBuilder stderr = new StringBuilder();
            int error_code = -1;
            //python.exe "contractor.py" "contract-examples/atm.xml" --cvc3-path="cvc3.exe" -o graphviz -f "contract-examples/atm.gv"
            using (Process graph = new Process())
            using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                graph.OutputDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                        outputWaitHandle.Set();
                    else
                        stdout.AppendLine(e.Data);
                };
                graph.ErrorDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                        errorWaitHandle.Set();
                    else
                        stderr.AppendLine(e.Data);
                };

                graph.StartInfo = start;
                LogManager.GetCurrentClassLogger().Info("START contractoring: {0}", filename);
                graph.Start();
                graph.BeginOutputReadLine();
                graph.BeginErrorReadLine();
                graph.WaitForExit();
                error_code = graph.ExitCode;
            }

            //Everything OK?
            if (error_code != 0)
            {
                throw new Exception(string.Format("Error creating the graph of the file {0}:\nstdout: {1}\nstderr: {2}\n", filename, stdout, stderr));
            }
        }

        private void RunDot(string filename)
        {
            //Create the folder for the images.
            string imagesPath = AppConfig.GetAbsolutePath(Config.Images);
            if (!Directory.Exists(imagesPath))
                Directory.CreateDirectory(imagesPath);

            //dot.exe contract-examples/atm.gv -Tsvg -o contract-examples/atm.svg

            //Transform the graphviz to an svg.
            var start = new ProcessStartInfo();
#if WIN
            start.FileName = Path.GetFileName(Config.DotPath);
#else
            start.FileName = Config.DotPath;
#endif
            start.Arguments = string.Format("\"{0}\" -Tsvg -o \"{1}\"",
                Path.Combine(Config.WorkingDirectoryPath, filename + ".gv"),
                Path.Combine(AppConfig.GetAbsolutePath(Config.Images), filename + ".svg"));
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;

            StringBuilder stdout = new StringBuilder();
            StringBuilder stderr = new StringBuilder();
            int error_code = -1;
            //dot.exe "contract-examples/atm.gv" -Tsvg -o "contract-examples/atm.svg"
            using (Process graph = new Process())
            using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                graph.OutputDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                        outputWaitHandle.Set();
                    else
                        stdout.AppendLine(e.Data);
                };
                graph.ErrorDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                        errorWaitHandle.Set();
                    else
                        stderr.AppendLine(e.Data);
                };
                graph.StartInfo = start;
                LogManager.GetCurrentClassLogger().Info("START dotting: {0}", filename);
                graph.Start();
                graph.BeginOutputReadLine();
                graph.BeginErrorReadLine();
                graph.WaitForExit();
                error_code = graph.ExitCode;
            }

            //Everything OK?
            if (error_code != 0)
            {
                throw new Exception(string.Format("Error creating the image of the file {0}:\nstdout: {1}\nstderr: {2}\n", filename, stdout, stderr));
            }
        }

        #region Helpers

        private FileInfo GetAndValidateExistingPath(Files request)
        {
            var targetFile = GetPath(request);
            if (!targetFile.Exists && !Directory.Exists(targetFile.FullName))
                throw new HttpError(HttpStatusCode.NotFound, new FileNotFoundException("Could not find: " + request.Path));

            return targetFile;
        }

        private FileResult GetFileResult(FileInfo fileInfo)
        {
            return new FileResult { Name = fileInfo.Name, Extension = fileInfo.Extension, FileSizeBytes = fileInfo.Length };
        }

        private FileInfo GetPath(Files request)
        {
            return new FileInfo(Path.Combine(Config.WorkingDirectoryPath, request.Path.GetSafePath()));
        }

        private FileInfo GetPath(FileStatus request)
        {
            return new FileInfo(Path.Combine(AppConfig.GetAbsolutePath(Config.Images), request.File.GetSafePath()));
        }

        private string RandomString(int length = 8, string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException("length", "length cannot be less than zero.");
            if (string.IsNullOrEmpty(allowedChars))
                throw new ArgumentException("allowedChars may not be empty.");

            const int byteSize = 0x100;
            if (byteSize < allowedChars.Length)
                throw new ArgumentException(String.Format("allowedChars may contain no more than {0} characters.", byteSize));

            // Guid.NewGuid and System.Random are not particularly random. By using a cryptographically-secure random number
            // generator, the caller is always protected, regardless of use.
            using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider())
            {
                var result = new StringBuilder();
                var buf = new byte[128];
                while (result.Length < length)
                {
                    rng.GetBytes(buf);
                    for (var i = 0; i < buf.Length && result.Length < length; ++i)
                    {
                        // Divide the byte into allowedCharSet-sized groups. If the random value falls into the last group and the
                        // last group is too small to choose from the entire allowedCharSet, ignore the value in order to avoid
                        // biasing the result.
                        var outOfRangeStart = byteSize - (byteSize % allowedChars.Length);
                        if (outOfRangeStart <= buf[i])
                            continue;
                        result.Append(allowedChars[buf[i] % allowedChars.Length]);
                    }
                }
                return result.ToString();
            }
        }

        #endregion Helpers
    }
}