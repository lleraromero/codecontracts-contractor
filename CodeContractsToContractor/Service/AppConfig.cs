﻿using System.IO;
using ServiceStack.Common.Utils;
using ServiceStack.Configuration;

namespace Service
{
  public class AppConfig
  {
    private IResourceManager resources;

    public AppConfig(IResourceManager resources)
    {
      this.resources = resources;
    }

    public string ContratorPath { get { return GetAbsolutePath(resources.GetString("ContractorPath")); } }

    public string CVC3Path { get { return GetAbsolutePath(resources.GetString("CVC3Path")); } }

    public string DotPath { get { return GetAbsolutePath(resources.GetString("DotPath")); } }

    public string PythonPath { get { return GetAbsolutePath(resources.GetString("PythonPath")); } }

    public string Images { get { return resources.GetString("Images"); } }

    public string ServerPort { get { return resources.GetString("ServerPort"); } }

    public string WorkingDirectoryPath { get { return GetAbsolutePath(resources.GetString("WorkingDirectory")); } }

    public static string GetAbsolutePath(string config)
    {
      return Path.GetFullPath(config.MapHostAbsolutePath().Replace('\\', Path.DirectorySeparatorChar));
    }
  }
}