﻿using System;
using System.Configuration;

namespace Service
{
  internal class Program
  {
      private static string ListeningOn { get { return string.Format("http://+:{0}/", ConfigurationManager.AppSettings["ServerPort"]); } }

    private static void Main(string[] args)
    {
      var appHost = new ContractorAppHost();
      appHost.Init();
      appHost.Start(ListeningOn);
      System.Console.WriteLine("AppHost Created at {0}, listening on {1}", DateTime.Now, ListeningOn);

      System.Console.WriteLine("Press ENTER to stop the server...");
      System.Console.ReadLine();
    }
  }
}