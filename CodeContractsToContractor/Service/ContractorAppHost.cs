﻿using System.IO;
using Funq;
using ServiceStack.Configuration;
using ServiceStack.Redis;
using ServiceStack.Redis.Messaging;
using ServiceStack.ServiceInterface.Cors;
using ServiceStack.WebHost.Endpoints;
using NLog;

namespace Service
{
  /// <summary>
  /// Create your ServiceStack web service application with a singleton AppHost.
  /// </summary>
  public class ContractorAppHost : AppHostHttpListenerBase
  {
    /// <summary>
    /// Initializes a new instance of your ServiceStack application, with the specified name and assembly containing the
    /// services.
    /// </summary>
    public ContractorAppHost()
      : base("Contractor Web Service", typeof(FilesService).Assembly)
    {
    }

    /// <summary>
    /// Configure the container with the necessary routes for your ServiceStack application.
    /// </summary>
    /// <param name="container">The built-in IoC used with ServiceStack.</param>
    public override void Configure(Container container)
    {
      //Permit modern browsers (e.g. Firefox) to allow sending of any REST HTTP Method
      Plugins.Add(new CorsFeature(allowedOrigins: "*", allowedMethods: "GET, POST",
                                  allowedHeaders: "cache-control,x-requested-with", allowCredentials: false));

      this.PreRequestFilters.Add((httpReq, httpRes) =>
      {
        //Handles Request and closes Responses after emitting global HTTP Headers
        if (httpReq.HttpMethod == "OPTIONS")
        {
          httpRes.AddHeader("Access-Control-Allow-Origin", "*");
          httpRes.AddHeader("Access-Control-Allow-Method", "GET, POST");
          httpRes.AddHeader("Access-Control-Allow-Headers", "cache-control,x-requested-with");
          httpRes.End();
        }
      });

      var config = new AppConfig(new ConfigurationResourceManager());
      container.Register(config);

      // Working directory for contractor and dot
      string workingDirectory = config.WorkingDirectoryPath;
      if (!Directory.Exists(workingDirectory))
        Directory.CreateDirectory(workingDirectory);

      // MQ Service to translate and plot the contracts asynchronously. This is necessary, since the ajax call can run
      // out of time due to a long wait.
      try
      {
          var mqHost = CreateMqServer();
          mqHost.RegisterHandler<FileRequest>(m => this.ServiceController.ExecuteMessage(m));
          mqHost.Start();
          container.Register(mqHost);
      }
      catch (System.Exception)
      {
          LogManager.GetCurrentClassLogger().Warn("Could not find a MQRedis server. Rollback to single thread execution.");
      }
    }

    private RedisMqServer CreateMqServer()
    {
      // TODO: Hacer el port un parametro.
      var redisFactory = new PooledRedisClientManager("localhost:6379");
      redisFactory.Exec(redis => redis.FlushAll());
      var mqHost = new RedisMqServer(redisFactory, 1);
      return mqHost;
    }
  }
}