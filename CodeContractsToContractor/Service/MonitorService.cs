﻿namespace Service
{
  public class MonitorService : ServiceStack.ServiceInterface.Service
  {
    public MonitorResponse Get(Monitor m)
    {
      Response.AddHeader("I-am-alive", "http://youtu.be/I_izvAbhExY");
      return new MonitorResponse() { Version = "0.0.0.3" };
    }
  }
}